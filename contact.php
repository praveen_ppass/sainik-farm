<?php 
include_once('header.php')
?>
   <div class="container-fluid banner-contact">
      <section class="wrapper">
         <div class="divider">
            <h1>Contact Us (Sainik Farms)</h1>
         </div>
      </section>
   </div>


   <div class="aboutdetails layout_padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">Contact Us </h3>
            </div>
         </div>

         <div class="row">
            <div class="col-md-3">
               <article class="imgBox">
                  <h5 class="icon icon-contactLocation">Corporate Address</h5>
                  <p class="phone"> SS Group Building, Salarpur U turn,
                     Opposite Chandra Heights, Dadri Main Rd,
                     Sector 101, Noida - 201301</p>
               </article>
            </div>
            <div class="col-md-3">
               <article class="imgBox">
                  <h5 class="icon icon-contactLocation">Site Address</h5>
                  <p class="phone">Sainik Farms, Sector 150, Noida 201301</p>
               </article>
            </div>
            <div class="col-md-3">
               <article class="imgBox">
                  <h5 class="icon icon-contactMobile">Call Us</h5>
                  <p class="phone"> Mob : Landline : 01206335922</p>
               </article>
            </div>
            <div class="col-md-3">
               <article class="imgBox">
                  <h5 class="icon icon-contactEmail">Mail Us</h5>
                  <p class="phone"> <a href="tel:contact@sainikfarmsnoida.com">contact@sainikfarmsnoida.com</a></p>
               </article>
            </div>
         </div>
      </div>
   </div>

   <div class="hiw_section write" style="background: #fff;">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">Write to us </h3>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="contact-form">
                  <form>
                     <input type="text" placeholder="Name" />
                     <input type="email" placeholder="Email" />
                     <textarea placeholder="Message"></textarea>
                     <button type="submit" class="blue_bt">Submit</button>

                  </form>
               </div>
            </div>
            <div class="col-md-6 text_align_center">
            <div id="googleMap" style="width:100%;height:400px;"></div>
               <!-- <img class="img-responsive" src="imgs/map.jpg" alt="#" /> -->
            </div>
         </div>
      </div>
   </div>
<?php 
include_once('footer.php')
?>