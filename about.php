<?php 
include_once('header.php')
?>
   <div class="container-fluid banner-background">
      <section class="wrapper">
         <div class="divider">
            <h1>About Sainik Farms Noida</h1>
         </div>
      </section>
   </div>


   <div class="aboutdetails layout_padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">About us</h3>

            </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <iframe class="star" src="https://www.youtube.com/embed/tgbNymZ7vqY">
               </iframe>
               <h3 style="text-transform: none !important">Welcome To Sainik Farms Noida</h3>
               <p>Sainik Farms is located at Sector-150, Noida,
                  known for its legacy of farm houses .Our Farms are
                  spread over 200 acres of lush green fields comprises of
                  raw earthy elegance, fully irrigated & vast fields that offer
                  a refreshing contrast to the myriad humdrum of city life.
                  Sainik Farms provide an eco-friendly environment for your friends,
                  families and colleagues and offers a break from hectic urban shuffle to refresh & rejuvenate.</p>
               <p>The venue is spread over massive 200 acre areas and
                  every detail has been carefully packaged into our uniquely
                  sophisticated concept. Among our trendy and contemporary properties
                  with water sprinkling lawns, swimming pool, green jogging track and a
                  lot more in the heart of Noida, we’re sure you’ll find the perfect destination
                  of your choice. As a real estate group poised to grow into a key developer,
                  we’re keen to showcase our capabilities in large developments. We focus on
                  delivering quality developments at levels and time committed. The tranquility
                  and serene environment of our farm will take you back in time and allow you to
                  experience the simplicity and peaceful existence of a simple village life.
                  The magnificent flowers, acres of landscaped garden, the soothing sound of birds,
                  the delicate aroma of herbs and the taste of our organic food will treat your senses
                  and our staff will ensure that your stay with us is indeed a memorable, one that you
                  will cherish for times to come.</p>

            </div>
         </div>

      </div>
   </div>
<?php 
include_once('footer.php')
?>