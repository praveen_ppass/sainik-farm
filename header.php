<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Sainik Farms in Noida Sector 150 &#8211; Farm Houses in Noida</title>
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/text.css">
   <link rel="stylesheet" href="css/viewbox.css">
   <link rel="stylesheet" href="css/banner.css">
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="css/fontawesome.min.css">
   <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
</head>

<body>
 <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="callbackPopup" role="dialog">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Request a callback</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <form>
                  <div class="modal-body">
                     <div class="form-group">
                        <input type="text" placeholder="Name" class="form-control" />
                     </div>
                     <div class="form-group">
                        <input type="text" placeholder="Phone" class="form-control" />
                     </div>
                     <div class="form-group">
                        <input type="email" placeholder="Email" class="form-control" />
                     </div>
                  </div>

                  <div class="modal-footer">
                     <button type="submit" class="btn-sm btn-success">Submit</button>
                  </div>
               </form>
            </div>
         </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
         <div class="modal-dialog modal-sm">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Download Brochure</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <form>
                  <div class="modal-body">
                     <div class="form-group">
                        <input type="text" placeholder="Name" class="form-control" />
                     </div>
                     <div class="form-group">
                        <input type="text" placeholder="Phone" class="form-control" />
                     </div>
                     <div class="form-group">
                        <input type="email" placeholder="Email" class="form-control" />
                     </div>
                  </div>

                  <div class="modal-footer">
                     <button type="submit" class="btn-sm btn-success">Submit</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <div id="header" class="header">
      <nav class="navbar navbar-expand-lg navbar-light text-capitalize">
         <div class="container">
            <a class="navbar-brand" href="index.php"><img src="imgs/logo.png" alt="" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#show-menu"
               aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="show-menu">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item active">
                     <a class="nav-link" href="index.php">Home</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="about.php">About Us</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" class="btnsss" href="index.php#project">Projects</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" class="btnsss" href="index.php#amenities">Amenities</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" class="btnsss" href="site-plan.php"> Project Site Plan</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="contact.php">Contact</a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
   </div>