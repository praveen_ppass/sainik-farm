<?php 
include_once('header.php')
?>

<div class="slider">
   <div id="main_slider" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
         <li data-target="#main_slider" data-slide-to="0" class="active"></li>
         <li data-target="#main_slider" data-slide-to="1"></li>
         <li data-target="#main_slider" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
         <div class="carousel-item active">
            <img class="d-block w-100 height" src="./imgs/slider/1.jpg" alt="slider_img">
            <div class="ovarlay_slide_cont">
               <h4>Sainik Farms Noida</h4>
               <p>Sainik Farms is located at Sector-150, Noida, known for its legacy of farm houses . Sainik Farms
                     provide an eco-friendly environment for your friends, families and colleagues and offers a break
                     from hectic urban shuffle to refresh & rejuvenate.
               </p>
               <a class="blue_bt" href="project.php">Click Here</a>
               </div>
            </div>
            <div class="carousel-item">
               <img class="d-block w-100 height" src="./imgs/slider/2.png" alt="slider_img">
               <div class="ovarlay_slide_cont">
                  <h4>Sainik Farms Haryana</h4>
                  <p>Sainik Farms is located at Haryana, known for its legacy of farm houses . Sainik Farms provide an
                     eco-friendly environment for your friends, families and colleagues and offers a break from hectic
                     urban shuffle to refresh & rejuvenate.
                  </p>
                  <a class="blue_bt" href="project.php">Click Here</a>
               </div>
            </div>
            <div class="carousel-item">
               <img class="d-block w-100 height" src="./imgs/slider/3.png" alt="slider_img">
               <div class="ovarlay_slide_cont">
                  <h4>Sainik Farms Noida</h4>
                  <p>Sainik Farms is located at Haryana, known for its legacy of farm houses . Sainik Farms provide an
                     eco-friendly environment for your friends, families and colleagues and offers a break from hectic
                     urban shuffle to refresh & rejuvenate.
                  </p>
                  <a class="blue_bt" href="project.php">Click Here</a>
               </div>
            </div>
            <div class="carousel-item">
               <img class="d-block w-100 height" src="./imgs/slider/4.png" alt="slider_img">
               <div class="ovarlay_slide_cont">
                  <h4>Sainik Farms Haryana</h4>
                  <p>Sainik Farms is located at Haryana, known for its legacy of farm houses . Sainik Farms provide an
                     eco-friendly environment for your friends, families and colleagues and offers a break from hectic
                     urban shuffle to refresh & rejuvenate.
                  </p>
                  <a class="blue_bt" href="project.php">Click Here</a>
               </div>
            </div>
      </div>
      <article class="mobilehide">
         <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
            <img src="imgs/left.png" alt="#" />
         </a>
         <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
            <img src="imgs/right.png" alt="#" />
         </a>
      </article>
   </div>
</div>

 <div id="project" class="container-fluid layout_padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">Our project</h3>
            </div>
         </div>
         <div class="row">
            <div class="col-md-4">
            <div class="card margin15">
            <div class="card-header">
               <img src="./imgs/slider/3.png" alt="Sainik Farms Noida">
            </div>
            <div class="card-body">
               <h4>Sainik Farms Noida</h4>
               <p>Sainik Farms is located at Sector-150, Noida, known for its legacy of farm houses .Our Farms are
                  spread over 200 acres of lush green fields comprises of raw earthy elegance, fully irrigated & vast
                  fields that offer a refreshing contrast to the myriad humdrum of city life. Sainik Farms provide an
                  eco-friendly environment for your friends, families and colleagues and offers a break from hectic
                  urban shuffle to refresh & rejuvenate.The venue is spread over massive 200 acre areas and every detail
                  has been carefully packaged into our uniquely sophisticated concept.
               </p>
              <a href="project.php" class="tag tag-travel">View Details</a>
            </div>
         </div>
            </div> 
            <div class="col-md-4">
            <div class="card">
            <div class="card-header">
               <img src="./imgs/slider/4.png" alt="Sainik Farms Haryana">
            </div>
            <div class="card-body">
               <h4>Sainik Farms Haryana</h4>
               <p>Sainik Farms is located at Haryana, known for its legacy of farm houses .Our Farms are spread over 200
                  acres of lush green fields comprises of raw earthy elegance, fully irrigated & vast fields that offer
                  a refreshing contrast to the myriad humdrum of city life. Sainik Farms provide an eco-friendly
                  environment for your friends, families and colleagues and offers a break from hectic urban shuffle to
                  refresh & rejuvenate.The venue is spread over massive 200 acre areas and every detail has been
                  carefully packaged into our uniquely sophisticated concept.
               </p>
               <a href="project.php" class="tag tag-travel">View Details</a>
            </div>
         </div>
            </div> 
         </div>
      </div>
   </div>

<div class="container-fluid layout_padding amenities" id="amenities">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="layout-title">Amenities</h3>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-8 col-sm-6 col-lg-3">
               <figure class="thumbnail-terry">
                  <img src="./imgs/amenities/Clubhouse.jpg" alt="" class="" />
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">Lavish Club House </h4>                      
                     </div>                    
                  </figcaption>
               </figure>
            </div>
            <div class="col-xs-8 col-sm-6 col-lg-3 offset-top-30 offset-sm-top-0">
               <figure class="thumbnail-terry"><a href="#">
                     <img src="./imgs/amenities/Swimming_pool.jpg" alt=""></a>
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">Swimming Pool </h4>
                     </div>                    
                  </figcaption>
               </figure>
            </div>
            <div class="col-xs-8 col-sm-6 col-lg-3 offset-top-30 offset-sm-top-0">
               <figure class="thumbnail-terry"><a href="#"><img src="./imgs/amenities/cricket-stadium.jpg" alt=""></a>
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">Cricket Stadium</h4>
                     </div>                 
                  </figcaption>
               </figure>
            </div>
            <div class="col-xs-8 col-sm-6 col-lg-3 offset-top-30 offset-sm-top-0">
               <figure class="thumbnail-terry"><a href="#"><img src="./imgs/amenities/greenarea.jpg" alt=""></a>
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">Green Area </h4>
                     </div>                  
                  </figcaption>
               </figure>
            </div>

         </div>
         <div class="row">
            <div class="col-xs-8 col-sm-6 col-lg-3">
               <figure class="thumbnail-terry"><a href="#"><img src="./imgs/amenities/tennis-court.jpg" alt=""></a>
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">Tennis Court </h4>
                     </div>                     
                  </figcaption>
               </figure>
            </div>
            <div class="col-xs-8 col-sm-6 col-lg-3 offset-top-30 offset-sm-top-0">
               <figure class="thumbnail-terry"><a href="#"><img src="./imgs/amenities/temple.jpg" alt=""></a>
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">Temple </h4>
                     </div>                     
                  </figcaption>
               </figure>
            </div>
            <div class="col-xs-8 col-sm-6 col-lg-3 offset-top-30 offset-sm-top-0">
               <figure class="thumbnail-terry"><a href="#"><img src="./imgs/amenities/Road-lights.jpg" alt=""></a>
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">Road Lights</h4>
                     </div>                    
                  </figcaption>
               </figure>
            </div>
            <div class="col-xs-8 col-sm-6 col-lg-3 offset-top-30 offset-sm-top-0">            
               <figure class="thumbnail-terry"><a href="#"><img src="./imgs/amenities/cctv-security.jpg" alt=""></a>
                  <figcaption>
                     <div>
                        <h4 class="thumbnail-terry-title">24x7 Security </h4>
                     </div>                  
                  </figcaption>
               </figure>
            </div>

         </div>
      </div>
   </div>

 <div class="container-fluid about_section layout_padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">About us</h3>

            </div>
         </div>
         <div class="row">
            <div class="col-md-5">
               <h3 style="text-transform: none !important">Welcome To Sainik Farms Noida</h3>
               <!-- <h4>Welcome To Sainik Farms Noida</h4> -->
               <p>Sainik Farms is located at Sector-150, Noida, known for its legacy of farm houses .Our Farms are
                  spread over 200 acres of lush green fields comprises of raw earthy elegance, fully irrigated & vast
                  fields that offer a refreshing contrast to the myriad humdrum of city life. Sainik Farms provide an
                  eco-friendly environment for your friends, families and colleagues and offers a break from hectic
                  urban shuffle to refresh & rejuvenate</p>
               <a class="blue_bt" href="#">Click Here</a>
            </div>
            <div class="col-md-6 offset-md-1">
               <div class="full text_align_center">
                  <img class="img-responsive" src="imgs/about.png" alt="#" />
               </div>
            </div>
         </div>
      </div>
   </div>
<div class="container-fluid farmhouse layout_padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">Own Your Farm House in Sainik Farms Noida</h3>
            </div>
            <div class="contbox text-center">
               <a target="_blank" href="whatsapp://send?text=Sainik Farms is located at Sector-150, Noida, known for its legacy of farm
                     houses" class="quote_btn">Whatsapp Share</a>
               <span class="or">or</span>
               <a href="#" class="call_btn" data-toggle="modal" data-target="#myModal">Download Brochure</a>
            </div>

         </div>
      </div>
   </div>

  <div class="container-fluid layout_padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">Our Clients</h3>
               <section class="customer-logos slider">
                  <div class="slide"><img src="./imgs/client/testimonials-1.png">
                  <p><strong>SAKSHI VERMA</strong></p>
                  <!-- <p>
                  My Dream Farm House Comes True. Spending Lots of time here.
                  </p> -->
                  </div>
                  <div class="slide"><img src="./imgs/client/testimonials-2.png">
                  <p><strong>SRIJAN SHARMA</strong></p>
                  <!-- <p>Beautiful Environment. Spending my most of the vacations here.</p> -->
                  </div>
                  <div class="slide"><img src="./imgs/client/testimonials-3.png">
                  <p><strong>RAHUL PANDEY</strong></p>
                  <!-- <p>True Greenery and peace. Shifted to my Farm House and enjoying.</p> -->
                  </div>
                  <!-- <div class="slide"><img
                        src="https://image.freepik.com/free-vector/colors-curl-logo-template_23-2147536125.jpg"></div>
                  <div class="slide"><img
                        src="https://image.freepik.com/free-vector/abstract-cross-logo_23-2147536124.jpg"></div>
                  <div class="slide"><img
                        src="https://image.freepik.com/free-vector/football-logo-background_1195-244.jpg"></div>
                  <div class="slide"><img
                        src="https://image.freepik.com/free-vector/background-of-spots-halftone_1035-3847.jpg"></div>
                  <div class="slide"><img
                        src="https://image.freepik.com/free-vector/retro-label-on-rustic-background_82147503374.jpg">
                  </div> -->
               </section>
            </div>
         </div>
      </div>
   </div>

 <div class="container-fluid legacyContiner layout_padding">
      <div class="container">
         <div class="row">
            <div class="col-md-12 text-center">
               <h3 class="layout-title">Legacy of Farm House</h3>
            </div>
         </div>
         <div class="row">
            <div class="col-md-5">
               <h3 style="text-transform: none !important">Connectivity</h3>
               <ul>
                  <li>Metro Station Sector 148 5min</li>
                  <li>Jewar Airport 30min</li>
                  <li>NPX Mall 5min</li>
                  <li>Noida Expressway 5min</li>
                  <li>Shaheed Bhagat Sigh Park 1min</li>
                  <li>Sector 144 ISBT 15min</li>
                  <li>Jaypee Sport City 15min</li>
                  <li>F1 Track 15min</li>
                  <li>Inox Cinema 15min</li>
                  <li>GrandVenice Mall 12min</li>
                  <li>Radission Hotel 10min</li>
               </ul>
               <h3 style="text-transform: none !important">School & Colleges</h3>
               <ul>
                  <li>Shiva Nadar School 15 Min</li>
                  <li>GD Goenka Public School 15 Min</li>
                  <li>Amity University 15 Min</li>
                  <li>Gautam Buddha University 15 Min</li>
               </ul>
               <h3 style="text-transform: none !important">Medicals</h3>
               <ul>
                  <li>Ivory Hospital 15</li>
                  <li>MinJaypee Hospital 15 Min</li>
                  <li>Fortis Hospital 45 Min</li>
                  <li>Apollo Hospital 45 Min</li>
                  <li>Educational Hub</li>
               </ul>
            </div>
            <div class="col-md-6 offset-md-1">
               <div class="full text_align_center">
                  <img class="img-responsive" src="imgs/location-map-901x1024.png" alt="#" />
               </div>
            </div>
         </div>
      </div>
   </div>
<?php 
include_once('footer.php')
?>