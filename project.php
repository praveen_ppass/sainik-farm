<?php 
include_once('header.php')
?>
<div class="project">
  <div class="image">
  <img  src="./imgs/slider/1.jpg" alt="Travel Image" />       
      <div class="container projectheader">
         <article class="row">
            <article class="col-md-9">
               <h4>Sainik Farms</h4>
               <p>Sainik Farms is located at Noida Sector-150,</p>
            </article>
            <article class="col-md-3 text-center">
               <button class="blue_bt full" data-toggle="modal" data-target="#myModal">
                  Download Brochure</button>
               <button class="blue_bt full blue" data-toggle="modal" data-target="#callbackPopup">Request a call back</button>
            </article>
         </article>               
      </div>
   </div>
</div>

   <div class="projectdetails layout_padding">
      <div class="container">      
         <div class="row">
            <div class="col-md-8">
               <div class="tabbable-panel">
                  <div class="tabbable-line">
                      <ul class="nav nav-tabs">
                        <li class="active" id="tabs_description">
                           <a href="#description" data-toggle="tab">
                              About Project </a>
                        </li>
                        <li id="tabs_video">
                           <a href="#video" data-toggle="tab">
                              Video </a>
                        </li>
                        <!-- <li>
                           <a href="#details" data-toggle="tab">
                              Details </a>
                        </li> -->
                        <li id="tabs_gallery">
                           <a href="#gallery" data-toggle="tab">
                              Gallery </a>
                        </li>
                        <li id="tabs_amenities">
                           <a href="#amenities" data-toggle="tab">
                              Amenities </a>
                        </li>
                        <li id="tabs_nearby">
                           <a href="#nearby" data-toggle="tab">
                              Near By </a>
                        </li>
                        <li id="tabs_bookatour">
                           <a href="#bookatour" data-toggle="tab">
                              Book a Tour </a>
                        </li>
                     </ul>
                     <div class="tab-content">
                        <div class="tab-pane in active" id="description">
                        <article class="tabwrapper">
                           <h5> Project Brief </h5>
                           <p>
                              SS Sainik farms in Sector-150 Noida, Noida by SS Group Noida is a residential project.</p>
                           <p>
                              SS Sainik farms price ranges from 33.97 Lacs to 1.40 Cr.
                           </p>
                           <p>
                              It also has amenities like Jogging track, Lawn tennis court and Swimming pool.
                              It also offers Car parking.
                           </p>
                           <p>

                              It is a ready to move project.
                           </p>
                           <p>
                              The project is spread over a total area of 200 acres of land. The construction is of 2
                              floors.
                              You can find SS Sainik farms price list on 99acres.com.
                              SS Sainik farms brochure is also available for easy reference.
                           </p>
                           <strong>  About City:</strong>
                           <p>
                              Backed by strong infrastructure and job creations, the real estate of Delhi/NCR is moving
                              up. With increasing
                              investments from builders in regions like Noida, Greater Noida, Gurgaon and Faridabad, the
                              increasing demands are being
                              catered to affecting the realty market positively. Additionally, other factors
                              contributing to this positivity are the
                              metro smoothening connectivity between different parts of the city and a number of
                              expressways and flyovers letting you
                              skip the traffic.
                              The positivity in the job market leading to migration of working professionals from around
                              the country to the city also
                              contributes to a positivity in the job market.
                              I'm interested in this project
                              SS Sainik farms Site ViewAll Photos9
                              SS Sainik farms Site ViewOutdoors6
                           </p>
                        </article>

                        </div>

                        <div class="imagegallery tab-pane fade" id="video">
                           <article class="tabwrapper">
                           <h4>Property Video</h4>
                              <iframe class="star" allowfullscreen="0" src="https://www.youtube.com/embed/tgbNymZ7vqY"> </iframe>
                           </article>                          
                        </div>

                        <div class="imagegallery tab-pane fade" id="gallery">
                           <article class="tabwrapper">
                           <h4>Gallery</h4>
                              <div class="carousel-container position-relative">  
                              
                              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                
                                 <a href="https://source.unsplash.com/tXqVe7oO-go/1600x900/" class="carousel-item active" data-slide-number="1">
                                    <img src="https://source.unsplash.com/tXqVe7oO-go/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/tXqVe7oO-go/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </a>
                                 <div class="carousel-item" data-slide-number="2">
                                    <img src="https://source.unsplash.com/qlYQb7B9vog/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/qlYQb7B9vog/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                                 <div class="carousel-item" data-slide-number="3">
                                    <img src="https://source.unsplash.com/QfEfkWk1Uhk/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/QfEfkWk1Uhk/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                                 <div class="carousel-item" data-slide-number="4">
                                    <img src="https://source.unsplash.com/CSIcgaLiFO0/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/CSIcgaLiFO0/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                                 <div class="carousel-item" data-slide-number="5">
                                    <img src="https://source.unsplash.com/a_xa7RUKzdc/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/a_xa7RUKzdc/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                                 <div class="carousel-item" data-slide-number="6">
                                    <img src="https://source.unsplash.com/uanoYn1AmPs/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/uanoYn1AmPs/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                                 <div class="carousel-item" data-slide-number="7">
                                    <img src="https://source.unsplash.com/_snqARKTgoc/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/_snqARKTgoc/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                                 <div class="carousel-item" data-slide-number="8">
                                    <img src="https://source.unsplash.com/M9F8VR0jEPM/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/M9F8VR0jEPM/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                                 <div class="carousel-item" data-slide-number="9">
                                    <img src="https://source.unsplash.com/Q1p7bh3SHj8/1600x900/" class="d-block w-100" alt="..." data-remote="https://source.unsplash.com/Q1p7bh3SHj8/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
                                 </div>
                              </div>
                              </div>

                              <!-- Carousel Navigation -->
                              <div id="carousel-thumbs" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                 <div class="carousel-item active">
                                    <div class="row mx-0">
                                    <div id="carousel-selector-0" class="thumb col-4 col-sm-2 px-1 py-2 selected" data-target="#myCarousel" data-slide-to="0">
                                       <img src="https://source.unsplash.com/Pn6iimgM-wo/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div id="carousel-selector-1" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="1">
                                       <img src="https://source.unsplash.com/tXqVe7oO-go/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div id="carousel-selector-2" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="2">
                                       <img src="https://source.unsplash.com/qlYQb7B9vog/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div id="carousel-selector-3" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="3">
                                       <img src="https://source.unsplash.com/QfEfkWk1Uhk/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div id="carousel-selector-4" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="4">
                                       <img src="https://source.unsplash.com/CSIcgaLiFO0/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div id="carousel-selector-5" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="5">
                                       <img src="https://source.unsplash.com/a_xa7RUKzdc/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    </div>
                                 </div>
                                 <div class="carousel-item">
                                    <div class="row mx-0">
                                   
                                    <div id="carousel-selector-7" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="7">
                                       <img src="https://source.unsplash.com/_snqARKTgoc/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div id="carousel-selector-8" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="8">
                                       <img src="https://source.unsplash.com/M9F8VR0jEPM/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div id="carousel-selector-9" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="9">
                                       <img src="https://source.unsplash.com/Q1p7bh3SHj8/600x400/" class="img-fluid" alt="...">
                                    </div>
                                    <div class="col-2 px-1 py-2"></div>
                                    <div class="col-2 px-1 py-2"></div>
                                    </div>
                                 </div>
                              </div>
                              <a class="carousel-control-prev" href="#carousel-thumbs" role="button" data-slide="prev">
                                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                 <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carousel-thumbs" role="button" data-slide="next">
                                 <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                 <span class="sr-only">Next</span>
                              </a>
                              </div>
</div>
                              <!-- /row -->
                              <a href="./imgs/projectsimg/1.jpg" class="image-link">
	<img src="./imgs/projectsimg/1.jpg" alt="">
</a>
<a href="images/i2.jpg" class="image-link" title="Image Title">
	<img src="images/i2-thumb.jpg" alt="">
</a>
<a href="images/i3.jpg" class="image-link">
	<img src="images/i3-thumb.jpg" alt="">
</a>
                           </article>                          
                        </div>
                        
                        <div class="projectrecords tab-pane fade" id="amenities">
                           <div class="tabwrapper">
                              <h4>Amenities</h4>
                              <p>
                                 While Purchasing Your Property and Farm House does depends on the locations and
                                 connectivity to
                                 essentials,
                                 transportation and entertainment. Sainik Farms offering you all these with the best
                                 costing
                                 ever.

                              </p>
                              <ul>
                                 <li> Swimming Pool: <span> Yes</span> </li>
                                 <li> Medical Center: <span> Yes</span> </li>
                                 <li> Solar Lighting: <span> Yes</span> </li>
                                 <li> Cricket Pitch: <span> Yes</span> </li>
                                 <li> Sun Deck: <span> Yes</span> </li>
                                 <li> Yoga/Meditatior: <span> Yes</span> </li>
                                 <li> Jogging Track: <span> Yes</span> </li>
                              </ul>
                           </div>
                        </div>  
                        
                        <div class="projectrecords tab-pane fade" id="nearby">
                           <div class="tabwrapper">
                              <h4>Our Location & Connectivity
                              </h4>
                              <p><strong>METRO STATION</strong></p>
                              <p>The Nearest Metro Station to is “Noida Aqua Line Metro Station” which is merely 5
                                 minutes from
                                 Sainik Farms.
                                 <strong>SECTOR 148</strong>
                              </p>

                              <p><strong>SPORTS CITY & CRICKET STADIUM</strong></p>
                              <p>Sports City and International Cricket Station is nearby Sainik Farms for sports loving
                                 people.
                              </p>

                              <p><strong>JEWAR INTERNATIONAL AIRPORT</strong></p>
                              <p>The Proposed Jewar Airport is about at 30 minutes from from Sainik Farms giving eased
                                 of
                                 connectivity.
                              </p>

                              <p><strong>SCHOOLS & UNIVERSITIES</strong></p>
                              <p>Sainik Farms is well connected with Schools and Universities. The Distance from
                                 different
                                 institutions are as follow:
                              </p>
                              <ol>
                                 <li>Shiva Nadar School 15 Min</li>
                                 <li>GD Goenka Public School 15 Min
                                 </li>
                                 <li>Amity University 15 Min</li>
                                 <li>Gautam Buddha University 15 Min</li>
                              </ol>

                              <p><strong>HOSPITALS</strong></p>
                              <p>The medical facilities too are well connected to Sainik Farms Noida. Different
                                 hospital’s
                                 connectivity are as follows:
                              </p>
                              <ol>
                                 <li>Ivory Hospital 15 Min</li>
                                 <li>Jaypee Hospital 15 Min
                                 </li>
                                 <li>Fortis Hospital 45 Min
                                 </li>
                                 <li>Apollo Hospital 45 Min
                                 </li>
                              </ol>

                              <p><strong>MALLS</strong></p>
                              <p>Sainik Farms is well connected to Grand Venice Mall and MSX Mall of Greater Noida which
                                 are at a
                                 distance of 15 min.
                                 from Sainik Farms.
                              </p>

                              <p><strong>MOVIE MULTIPLEXES</strong></p>
                              <p>Multiple Multiplexes are connected well to Sainik Farms for Entertainment. Inox and PVR
                                 are at
                                 distance of 15 min from
                                 our Farm Houses.

                              </p>

                              <p><strong>AMUSEMENT PARK
                                 </strong></p>
                              <p>World of Wonders is now Opened in Noida as complete amusement park for you to release
                                 your
                                 stress any day.

                              </p>
                           </div>
                        </div>

                        <div class="projectrecords tab-pane fade" id="bookatour">
                           <div class="contact-form projecttour">
                              <h4>Schedule a Tour</h4>
                              <form>
                                 <input type="text" placeholder="Name" />
                                 <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                       <input type="text" placeholder="Phone" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <input type="email" placeholder="Email" />
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                       <input type="date" placeholder="Tour Date" />
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                       <input type="time" placeholder="Tour Time" />
                                    </div>
                                 </div>
                                 <textarea placeholder="Message"></textarea>
                                 <button type="submit" class="blue_bt">Submit</button>
                              </form>
                           </div>
                        </div>
                     </div>  
                     <div class="imagegallery" id="video">
                        <div class="card">
                           <h4>Price & Payment</h4>
                           <img class="star img-responsive" src="./imgs/price.png">
                        </div>
                     </div>
                     <div class="imagegallery" id="video">
                        <div class="card">
                           <h4>SS Greens Site Plan (Sainik Farms)</h4>
                           <img class="star img-responsive" src="./imgs/Sainik.png">
                        </div>
                     </div>
                     <div class="imagegallery" id="video">
                        <div class="card">
                           <h4>Units:</h4>
                           <div class="row">
                              <div class="col col-md-4 col-sm-12 text-center">
                                 <img class="star img-responsive" src="./imgs/72.png"> </img>
                                 <p>Size 1 : 72.6 x 150 feet(1210 Sq. Yards)</p>
                              </div>
                              <div class="col col-md-4 col-sm-12 text-center">
                                 <img class="star img-responsive" src="./imgs/150.png"> </img>
                                 <p>Size 2 : 150 x 150 feet(2500 Sq. Yards)</p>
                              </div>
                              <div class="col col-md-4 col-sm-12 text-center">
                                 <img class="star img-responsive" src="./imgs/300.png"> </img>
                                 <p>Size 3 : 300 x 300 feet(5000 Sq. Yards)
                                 </p>
                              </div>
                           </div>

                        </div>
                     </div>              
                    
                  </div>
               </div>

            </div>
            <div class="col-md-4">
               <div class="card margin15">
                  <div class="card-header">
                     <h4> Recently Added</h4>
                  </div>
                  <div class="card-body">
                     <div class="details">
                        <article class="imageWrapper">
                           <img src="./imgs/slider/3.png" alt="Sainik Farms Noida" />
                        </article>
                        <article class="text">
                           <h4>Sainik Farms Noida</h4>
                           <p>Sainik Farms is located at Sector-150,</p>
                        </article>
                     </div>
                     <!-- <div class="details">
                        <article class="imageWrapper">
                           <img src="./imgs/slider/4.png" alt="Sainik Farms Noida"/>
                        </article>
                        <article class="text">
                           <h4>Sainik Farms Noida</h4>
                           <p>Sainik Farms is located at Sector-150,</p>
                        </article>
                     </div> -->
                   
                  </div>
               </div>  
               <div class="card">
                  <div class="card-header">
                     <h4> Map</h4>
                  </div>
                  <div class="card-body">
                     <div class="details">
                        <div id="googleMap" style="width:100%;height:400px;"></div>
                     </div>
                  </div>
               </div>  
            </div>
         </div>
      </div>
   </div>

<?php 
include_once('footer.php')
?>