<?php 
include_once('header.php')
?>
<div class="project">
  <div class="image">
  <img src="./imgs/slider/2.png" alt="Farm house Image" />       
      <div class="container projectheader">
         <article class="row">
            <article class="col-md-9">
               <h4>SS Greens Site Plan (Sainik Farms)</h4>
            
            </article>
            <article class="col-md-3 text-center">
               <button class="blue_bt full" data-toggle="modal" data-target="#myModal">
                  Download Brochure</button>
               <button class="blue_bt full blue" data-toggle="modal" data-target="#callbackPopup">Request a call back</button>
            </article>
         </article>               
      </div>
   </div>
</div>

   <div class="projectdetails layout_padding">
      <div class="container">      
         <div class="row">
            <div class="col-md-8">
               <div class="tabbable-panel">
                  <div class="tabbable-line">
                     <div class="imagegallery">
                        <div class="card">
                          <img class="star img-responsive" src="./imgs/Sainik.png">
                         </div>
                     </div>
                     <div class="imagegallery">
                        <div class="card">
                           <h4>Units:</h4>
                           <div class="row">
                              <div class="col col-md-4 col-sm-12 text-center">
                                 <img class="star img-responsive" src="./imgs/72.png"> </img>
                                 <p>Size 1 : 72.6 x 150 feet(1210 Sq. Yards)</p>
                              </div>
                              <div class="col col-md-4 col-sm-12 text-center">
                                 <img class="star img-responsive" src="./imgs/150.png"> </img>
                                 <p>Size 2 : 150 x 150 feet(2500 Sq. Yards)</p>
                              </div>
                              <div class="col col-md-4 col-sm-12 text-center">
                                 <img class="star img-responsive" src="./imgs/300.png"> </img>
                                 <p>Size 3 : 300 x 300 feet(5000 Sq. Yards)
                                 </p>
                              </div>
                           </div>

                        </div>
                     </div>              
                    
                  </div>
               </div>

            </div>
            <div class="col-md-4">
           
           <div class="card">
                           <div class="contact-form projecttour" style="box-shadow: none;">
                              <h4>Schedule a Tour</h4>
                              <form>
                                 <input type="text" placeholder="Name" />
                                 <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                       <input type="text" placeholder="Phone" />
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                       <input type="email" placeholder="Email" />
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                       <input type="date" placeholder="Tour Date" />
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                       <input type="time" placeholder="Tour Time" />
                                    </div>
                                 </div>
                                 <textarea placeholder="Message"></textarea>
                                 <button type="submit" class="blue_bt">Submit</button>
                              </form>
                           </div>
                        </div>
          
               <div class="card">
                  <div class="card-header">
                     <h4> Map</h4>
                  </div>
                  <div class="card-body">
                     <div class="details">
                        <div id="googleMap" style="width:100%;height:400px;"></div>
                     </div>
                  </div>
               </div>  
            </div>
         </div>
      </div>
   </div>

<?php 
include_once('footer.php')
?>