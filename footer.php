<div class="socialbox">
   <div class="social">
      <a href="#" target="_blank" class="facebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
      <a href="#" target="_blank" class="twitter" title="Twitter"><i class="fab fa-twitter"></i></a>
      <a href="#" target="_blank" class="linkedin" title="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
      <a href="#" target="_blank" class="instagram" title="Instagram"><i class="fab fa-instagram"></i></a>
   </div>
</div>

<div class="container-fluid footer">
   <div class="container">
      <div class="row">
         <div class="col-lg-6 col-md-6 col-12">
            <div class="footer_blog_section">
               <h4 class="text-uppercase">Our offer</h4>
               <p style="margin-top: 5px;">Sainik Farms is located at Sector-150, Noida, known for its legacy of farm
                     houses .Our Farms are spread over 200 acres of lush green fields comprises of raw earthy elegance,
                     fully irrigated & vast fields that offer a refreshing contrast to the myriad humdrum of city life.
                  </p>
            </div>
         </div>

          
         <div class="col-lg-3 col-md-6 col-12">
            <div class="item">
               <h4 class="text-uppercase">Contact Info</h4>
               <strong>Corporate Office Address:</strong>           
               <p> SS Group Building, Salarpur U turn, Opposite Chandra Heights, Dadri Main Rd,</p>
               <strong>Customer Service:</strong>             
               <p>Landline : 01206335922</p>
            </div>
         </div>
         <div class="col-lg-2 col-md-6 col-12">
            <div class="item">
               <h4 class="text-uppercase">Our offer</h4>
               <ul>
                  <li><a href="about.php">About</a></li>
                  <li><a href="contact.php">Contact</a></li>
                  <li><a href="site-plan.php">Project Site Plan</a></li>
                  <!-- <li><a href="#">Project Payment Plan</a></li> -->
               </ul>
            </div>
         </div>  
      </div>
      <div class="row">
         <div class="copyright text-center">
            <p>Copyright © 2020 Sainik Farms </p>
         </div>
      </div>
   </div>
</div>

  

   <script src="js/jquery-3.3.1.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
   <script src="js/slider.js"></script>
   <script src="js/jquery.viewbox.min.js"></script>
   <script>
if ($(window).width() > 992) {
  $(window).scroll(function(){  
     if ($(this).scrollTop() > 40) {
        $('#header').addClass("fixed-top");
        // add padding top to show content behind navbar
        $('body').css('padding-top', $('.navbar').outerHeight() + 'px');
      }else{
        $('#header').removeClass("fixed-top");
         // remove padding top from body
        $('body').css('padding-top', '0');
      }   
  });
} // end if
   </script>
   <script>
   $('.carousel-item').viewbox({

// template
template: '<div class="viewbox-container"><div class="viewbox-body"><div class="viewbox-header"></div><div class="viewbox-content"></div><div class="viewbox-footer"></div></div></div>',

// loading spinner
loader: '<div class="loader"><div class="spinner"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>',

// show title
setTitle: true,

// margin in px
margin: 20,

// duration in ms
resizeDuration: 300,
openDuration: 200,
closeDuration: 200,

// show close button
closeButton: true,

// show nav buttons
navButtons: true,

// close on side click
closeOnSideClick: true,

// go to next image on content click
nextOnContentClick: true,

// enable touch gestures
useGestures: true,

// image extensions
// used to determine if a target url is an image file
imageExt: ['png','jpg','jpeg','webp','gif']

});
   </script>
<script>
function myMap() {
 var mapCanvas = document.getElementById("googleMap");
  var myCenter = new google.maps.LatLng(28.4140675,77.4787182); 
  var mapOptions = {center: myCenter, zoom: 5};
  var map = new google.maps.Map(mapCanvas,mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter,
    animation: google.maps.Animation.BOUNCE
  });
  marker.setMap(map);

}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCm4m_PNs3Y1QOFsXuLbTYIE90xewNDM20&callback=myMap"></script>
   <script>
      $(function () {

         'use strict';

         var winH = $(window).height();

         $('header').height(winH);

         $('header .container > div').css('top', (winH / 2) - ($('header .container > div').height() / 2));

         $('.navbar ul li a.search').on('click', function (e) {
            e.preventDefault();
         });
         $('.navbar a.search').on('click', function () {
            $('.navbar form').fadeToggle();
         });

         $('.btnsss').on('click', function (e) {

            var getAttr = $(this).attr('href');

            e.preventDefault();
            $('html').animate({ scrollTop: $(getAttr).offset().top }, 30000);
         });
            $('.nav-tabs a').on('click', function (e) {
            var getIdAttr = $(this).attr('href').replace("#","");
            $('.nav-tabs li').removeClass('active');
            $("#tabs_"+getIdAttr).addClass('active');
         });
      })

      $(function(){
		
      $('#thumbnail li').click(function(){
         var thisIndex = $(this).index()
            
         if(thisIndex < $('#thumbnail li.active').index()){
            prevImage(thisIndex, $(this).parents("#thumbnail").prev("#image-slider"));
         }else if(thisIndex > $('#thumbnail li.active').index()){
            nextImage(thisIndex, $(this).parents("#thumbnail").prev("#image-slider"));
         }
            
         $('#thumbnail li.active').removeClass('active');
         $(this).addClass('active');
   
         });
         
      });
   
   var width = $('#image-slider').width();
   
   function nextImage(newIndex, parent){
      parent.find('li').eq(newIndex).addClass('next-img').css('left', width).animate({left: 0},600);
      parent.find('li.active-img').removeClass('active-img').css('left', '0').animate({left: -width},600);
      parent.find('li.next-img').attr('class', 'active-img');
   }
   function prevImage(newIndex, parent){
      parent.find('li').eq(newIndex).addClass('next-img').css('left', -width).animate({left: 0},600);
      parent.find('li.active-img').removeClass('active-img').css('left', '0').animate({left: width},600);
      parent.find('li.next-img').attr('class', 'active-img');
   }
   
   /* Thumbails */
   var ThumbailsWidth = ($('#image-slider').width() - 18.5)/7;
   $('#thumbnail li').find('img').css('width', ThumbailsWidth);
   
   
   </script>
</body>